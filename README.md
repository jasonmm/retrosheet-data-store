# Retrosheet Data Store

This project can be used to create a filesystem based Retrosheet data store. This allows for analysis to be run on the Retrosheet data without needing to download or re-process Retrosheet files every time.

## Usage

### Setup

Create a directory that will store the Retrosheet data.

```shell script
mkdir -p ~/data/Retrosheet
```

### Build

To process the all the years between 1990 and 2000 (inclusive) use:

```shell script
build.sh ~/data/Retrosheet `seq 1990 2000`
```

To process just one year use:

```shell script
build.sh ~/data/Retrosheet 1985
```

### Reprocessing

In order to reprocess a year, delete that year's TEAM file.  For example,

```shell script
rm eve/1985/TEAM1985 post/1985/TEAM1985
```

Then rerun `build.sh` passing in the deleted years. This will cause the corresponding zip files in _zips/_ to be unzipped and overwrite the existing event files.

In order to have the zip files re-downloaded from Retrosheet delete those files from the _zips/_ directory as well.

```shell script
rm zips/1985eve.zip zips/1985post.zip
```
