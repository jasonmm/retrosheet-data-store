#!/usr/bin/env bash

log() {
  MESSAGE=$1
  echo "$(date +"%Y%m%d %H:%m:%S") $MESSAGE"
}

usage() {
  echo "Usage: $0 <retrosheet store directory> <year> <year> ..."
  echo "   or: $0 <retrosheet store directory> \`seq -s ' ' <start year> <end year>\`"
}

if [ $# -lt 2 ]; then
  usage
  exit
fi

RETROSHEET_SOURCE=$1
#YEAR=$2
shift
SUFFIXES='eve post'

if [ ! -d "$RETROSHEET_SOURCE" ]; then
  echo "!!! $RETROSHEET_SOURCE is not a directory. The $RETROSHEET_SOURCE directory must exist before running this script."
  exit 1
fi

log "Using Retrosheet data in $RETROSHEET_SOURCE."

mkdir -p "$RETROSHEET_SOURCE"/zips
mkdir -p "$RETROSHEET_SOURCE"/sportsml
mkdir -p "$RETROSHEET_SOURCE"/eve
mkdir -p "$RETROSHEET_SOURCE"/post

for YEAR in "$@"; do
  for SUFFIX in $SUFFIXES; do
    EVENTFILE_DIRECTORY=$RETROSHEET_SOURCE/$SUFFIX/${YEAR}
    SPORTSML_DIRECTORY=$RETROSHEET_SOURCE/sportsml/${YEAR}

    mkdir -p "$EVENTFILE_DIRECTORY"
    mkdir -p "$SPORTSML_DIRECTORY"

    # cwbox requires that it be run in the same directory as the event files.
    log "Entering $EVENTFILE_DIRECTORY"
    cd "$EVENTFILE_DIRECTORY" || exit

    # If the zip file doesn't exist, download it and unzip it to
    # the $EVENTFILE_DIRECTORY.
    ZIP_FILENAME=${YEAR}$SUFFIX.zip
    if [ ! -f "$RETROSHEET_SOURCE/zips/$ZIP_FILENAME" ]; then
      log "Downloading $ZIP_FILENAME"
      wget -q "https://www.retrosheet.org/events/$ZIP_FILENAME"

      if [ ! -f "$ZIP_FILENAME" ]; then
        log "!!! Error downloading Retrosheet zip file."
        exit
      fi

      mv "$ZIP_FILENAME" "$RETROSHEET_SOURCE/zips/"
    fi

    # If the TEAM file does not exist then unzip the zip file.
    if [ ! -f "TEAM${YEAR}" ]; then
      log "Unzipping $RETROSHEET_SOURCE/zips/$ZIP_FILENAME"
      unzip -qo "$RETROSHEET_SOURCE/zips/$ZIP_FILENAME" -d "$EVENTFILE_DIRECTORY"
    fi

    for EVENTFILE in *.EV*; do
      log "Processing Event File $EVENTFILE"

      dos2unix -q "$EVENTFILE"

      # For each game in the event file run cwbox unless the corresponding
      # SportsML file already exists.
      grep '^id,' "$EVENTFILE" | cut -d , -f 2 | while IFS= read -r GAMEID; do
        XML_FILENAME=$RETROSHEET_SOURCE/sportsml/${YEAR}/$GAMEID.xml
        if [ ! -f "$XML_FILENAME" ]; then
          log "Year: ${YEAR} | Event File: $EVENTFILE | Game: $GAMEID | XML Output: $XML_FILENAME"
          cwbox -q -i "$GAMEID" -y "${YEAR}" -S "$EVENTFILE" >"$XML_FILENAME"
        fi
      done
    done
  done
done
